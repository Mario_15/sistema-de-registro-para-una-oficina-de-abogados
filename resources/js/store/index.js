import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './Auth'
import post from './post'
import category from './category'
import comments from './comments'

Vue.use(Vuex)
export default new Vuex.Store({
    modules:{
        Auth,
        post,
        category,
        comments,

    }
})