<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//

use App\Models\User;
use App\Models\Category;
use App\Models\Comment;
class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'numerodecaso',
        'slug',
        'body',
        'category_id',
        'user_id',
        'photo'
    ];

    public function user(){
        return $this->belongsTo(user::class);

    }
    public function category(){
        return $this->belongsTo(Category::class);

    }
    public function comments(){
        return $this->hasMany(Comment::class);

    }
    public function getRouteKeyName()
    {
    return 'slug';
    }
    public function scopeLatest($query){
        return $query->orderBy('created_at','DESC');
    }
}
